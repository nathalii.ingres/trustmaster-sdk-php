<?php

namespace RESWUE\Trust;


use RESWUE\Trust\TrustInformation\Summary;
use RESWUE\Trust\TrustInformation\Trust;

class TrustInformation
{
    private $trustmasterId;
    /**
     * @var Trust
     */
    private $trust;
    /**
     * @var Trust
     */
    private $newestTrust;
    /**
     * @var Summary
     */
    private $summary;
    /**
     * @var Summary
     */
    private $summaryUnverified;
    private $generation;

    /**
     * TrustInformation constructor.
     * @param array $data
     * @throws \Exception
     */
    public function __construct(array $data)
    {
        if (isset($data['trustmaster_id'])) {
            $this->setTrustmasterId($data['trustmaster_id']);
        }
        if (isset($data['trust'])) {
            $this->setTrust(new Trust($data['trust']));
        }
        if (isset($data['newest_trust'])) {
            $this->setNewestTrust(new Trust($data['newest_trust']));
        }
        if (isset($data['summary'])) {
            $this->setSummary(new Summary($data['summary']));
        }
        if (isset($data['summary_unverified'])) {
            $this->setSummaryUnverified(new Summary($data['summary_unverified']));
        }
        if (isset($data['generation'])) {
            $this->setGeneration($data['generation']);
        }
    }

    /**
     * @return string
     */
    public function getTrustmasterId()
    {
        return $this->trustmasterId;
    }

    /**
     * @param string $trustmasterId
     */
    public function setTrustmasterId($trustmasterId): void
    {
        $this->trustmasterId = $trustmasterId;
    }

    /**
     * @return Trust|null
     */
    public function getTrust(): ?Trust
    {
        return $this->trust;
    }

    /**
     * @param Trust $trust
     */
    public function setTrust(Trust $trust): void
    {
        $this->trust = $trust;
    }

    /**
     * @return Trust|null
     */
    public function getNewestTrust(): ?Trust
    {
        return $this->newestTrust;
    }

    /**
     * @param Trust $newestTrust
     */
    public function setNewestTrust(Trust $newestTrust): void
    {
        $this->newestTrust = $newestTrust;
    }

    /**
     * @return Summary
     */
    public function getSummary(): Summary
    {
        return $this->summary;
    }

    /**
     * @param Summary $summary
     */
    public function setSummary(Summary $summary): void
    {
        $this->summary = $summary;
    }

    /**
     * @return Summary
     */
    public function getSummaryUnverified(): Summary
    {
        return $this->summaryUnverified;
    }

    /**
     * @param Summary $summaryUnverified
     */
    public function setSummaryUnverified(Summary $summaryUnverified): void
    {
        $this->summaryUnverified = $summaryUnverified;
    }

    /**
     * @return integer
     */
    public function getGeneration()
    {
        return $this->generation;
    }

    /**
     * @param integer $generation
     */
    public function setGeneration($generation): void
    {
        $this->generation = $generation;
    }


}
