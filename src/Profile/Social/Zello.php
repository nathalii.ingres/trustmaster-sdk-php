<?php

namespace RESWUE\Trust\Profile\Social;


class Zello
{
    private $username;

    public function __construct(array $data)
    {
        if (isset($data['username'])) {
            $this->setUsername($data['username']);
        }
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername($username): void
    {
        $this->username = $username;
    }
}