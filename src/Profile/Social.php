<?php

namespace RESWUE\Trust\Profile;


use RESWUE\Trust\Profile\Social\Google;
use RESWUE\Trust\Profile\Social\Telegram;
use RESWUE\Trust\Profile\Social\Zello;

class Social
{
    private $google = [];
    private $telegram = [];
    private $zello = [];

    public function __construct($data)
    {
        if (isset($data['google'])) {
            foreach ($data['google'] AS $google) {
                $this->addGoogle(new Google($google));
            }
        }

        if (isset($data['telegram'])) {
            foreach ($data['telegram'] AS $telegram) {
                $this->addTelegram(new Telegram($telegram));
            }
        }

        if (isset($data['zello'])) {
            foreach ($data['zello'] AS $zello) {
                $this->addZello(new Zello($zello));
            }
        }
    }

    /**
     * @return Google[]
     */
    public function getGoogle()
    {
        return $this->google;
    }

    protected function addGoogle(Google $google)
    {
        $this->google[] = $google;
    }

    /**
     * @return Telegram[]
     */
    public function getTelegram()
    {
        return $this->telegram;
    }

    protected function addTelegram(Telegram $telegram)
    {
        $this->telegram[] = $telegram;
    }

    /**
     * @return Zello[]
     */
    public function getZello()
    {
        return $this->zello;
    }

    protected function addZello(Zello $zello)
    {
        $this->zello[] = $zello;
    }


}