<?php

namespace RESWUE\Trust;

use Firebase\JWT\JWT;

/**
 * \RESWUE\Trust\AccessToken represents an OAuth access token.
 */
class AccessToken {
    protected $token;
    protected $refreshToken;
    protected $expiresIn;

    /**
     * The public key used to sign the JWT.
     */
    const PUBLIC_KEY = <<<'EOT'
-----BEGIN PUBLIC KEY-----
MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAm/2hdc1k6k7xszAUH6OU
kWwZXnyzxH0zhMzUulP5IgUCDTGuchswVq3y/FGuj0+dNirMKBmFelHS0kqLzirQ
x1V2hjjU3O9PTiwZQaeHbYmPAKA+oba1DjGRuDV8gj59ymMHysBsujs1tQ8uP5Sp
BVfWLzCBDkzTMoEpw8R99igmhEn6PW1ga1Nh5rzbtCxtrgbAwDaMKC0dhNTgcBfU
3DWtjsYDoqkATLR/awmtJgWJl6gswgcKCzgULG7zE0v/J0rDOi0cF+lYQZyHdUm1
VWiGOFLHxzl65mspxygCCkLdoW0FazE80JqrAdtY3b2KIRV/fToN6n0VNKbU7+Qq
BSHUvk4auODZgBSbT5tk/xtVlv8q/qn7pwZ3JxGjbVDgF0Sh5qba8d/A2xhnZZHJ
rbH9WlVYuBDAZBhh7UEIMBXYCiwZgLz1j6F0QoGOve39hsIyaDQng1r2vx/IvAnY
rxkH+/e23glyrZguX+zvD1nP09XKVvkKqew1UmBXtszVqXX4iRQGURrIfd5q/+F1
NdsyAYkXJU/fYKxhJztWrmhrPFzzLq/7R0zhl+09sNyBi5ATWsgJTQTgf/NjXL3Z
uhnJqJ4FA6oWg2PBlv9OJ2p3+Kv2WjexrAvAoJdVDtd2iT05NU/eONo7/H1hd0bk
ieXjxeIoO1KOYlwcRnnMgukCAwEAAQ==
-----END PUBLIC KEY-----
EOT;

    public function __construct($data)
    {
        if (!isset($data['access_token'])) throw new \InvalidArgumentException('access_token is missing from $data array.');

        $this->token = $data['access_token'];
        $this->getDecodedToken();

        if (isset($data['refresh_token'])) {
            $this->refreshToken = $data['refresh_token'];
        }

        if (isset($data['expires_in'])) {
            $this->expiresIn = $data['expires_in'];
        }
    }

    /**
     * Returns the raw JWT string.
     *
     * @return  string
     */
    public function getToken()
    {
        $this->getDecodedToken();
        return $this->token;
    }

    /**
     * @see \RESWUE\Trust\AccessToken::getToken()
     */
    public function __toString()
    {
        return $this->getToken();
    }

    /**
     * Returns the object contained in the JWT.
     *
     * @return  \stdClass
     */
    public function getDecodedToken()
    {
        return JWT::decode($this->token, self::PUBLIC_KEY, ['RS256']);
    }

    /**
     * @return string
     */
    public function getRefreshToken()
    {
        return $this->refreshToken;
    }

    /**
     * @return string
     */
    public function getExpiresIn()
    {
        return $this->expiresIn;
    }
}
