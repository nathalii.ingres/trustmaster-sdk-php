<?php

namespace RESWUE\Trust;

use RESWUE\Trust\GroupMemberships\Membership;

class GroupMemberships implements \IteratorAggregate
{
    private $memberships = [];

    public function __construct($data)
    {
        if (isset($data['groups'])) {
            foreach ($data['groups'] AS $membership) {
                $this->addMembership(new Membership($membership));
            }
        }
    }

    /**
     * @inheritDoc
     */
    public function getIterator(): \Traversable
    {
        return new \ArrayIterator($this->memberships);
    }

    /**
     * @return Membership[]
     */
    public function getMemberships()
    {
        return $this->memberships;
    }

    /**
     * @param Membership $group
     */
    protected function addMembership(Membership $membership)
    {
        $this->memberships[] = $membership;
    }
}
