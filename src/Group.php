<?php

namespace RESWUE\Trust;

class Group
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $joinUrl;

    public function __construct($data)
    {
        if (isset($data['name'])) {
            $this->setName($data['name']);
        }

        if (isset($data['join_url'])) {
            $this->setJoinUrl($data['join_url']);
        }
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getJoinUrl(): ?string
    {
        return $this->joinUrl;
    }

    /**
     * @param string $joinUrl
     */
    public function setJoinUrl(string $joinUrl): void
    {
        $this->joinUrl = $joinUrl;
    }
}