<?php

namespace RESWUE\Trust;


class ExternalProfile
{
    private $url;

    public function __construct(array $data)
    {
        if (isset($data['external_profile'])) {
            $this->setUrl($data['external_profile']);
        }
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url): void
    {
        $this->url = $url;
    }
}