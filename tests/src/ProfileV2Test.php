<?php

namespace RESWUE\TrustTest;


use PHPUnit\Framework\TestCase;
use RESWUE\Trust\Profile\Location;
use RESWUE\Trust\Profile\Social;

class ProfileV2Test extends TestCase
{
    public function testFullProfile()
    {
        $data = json_decode(file_get_contents(__DIR__ . '/../assets/fullprofile_v2.json'), true);
        $profile = new \RESWUE\Trust\ProfileV2($data);

        $this->assertEquals('RESWUE', $profile->getAgentName());
        $this->assertEquals('reswue@example.com', $profile->getEmail());
        $this->assertEquals('RESWUE', $profile->getGoogleName());
        $this->assertEquals('110477265576843912947', $profile->getGoogleId());
        $this->assertEquals('https://lh6.googleusercontent.com/-QyLXP7Ml4Eg/AAAAAAAAAAI/AAAAAAAAABI/cfZ6TST4sCU/photo.jpg?sz=50', $profile->getGoogleAvatar());

        $this->assertEquals('58.009233', $profile->getLocationLat());
        $this->assertEquals('-62.566417', $profile->getLocationLng());
        $this->assertEquals('rounded', $profile->getLocationPrivacy());

        $this->assertEquals('RESWUE', $profile->getZelloUsername());

        $this->assertEquals('foo', $profile->getTelegramUsername());
        $this->assertEquals('123456789', $profile->getTelegramId());

    }
}