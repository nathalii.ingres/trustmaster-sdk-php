<?php

namespace RESWUE\TrustTest;


use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use RESWUE\Trust\AccessToken;
use RESWUE\Trust\Client;
use RESWUE\Trust\Profile;
use RESWUE\Trust\ProfileV2;

class ClientTest extends TestCase
{
    private $token;

    public function setUp(): void
    {
        parent::setUp();

        $this->token = \Mockery::mock(AccessToken::class);
        $this->token->shouldReceive('getToken')->withAnyArgs()->andReturns('randomtext');
    }

    public function testMe()
    {
        $response = \Mockery::mock(ResponseInterface::class);
        $response->shouldReceive('getBody')->withAnyArgs()->andReturns(
            file_get_contents(__DIR__ . '/../assets/fullprofile.json')
        );

        $httpClient = \Mockery::mock(\GuzzleHttp\Client::class);
        $httpClient->shouldReceive('request')->withAnyArgs()->andReturns($response);


        $client = new Client('123','456', null, 4);
        $client->setHttpClient($httpClient);
        $profile = $client->me($this->token);

        $this->assertInstanceOf(Profile::class, $profile);
    }

    public function testMeV2()
    {
        $response = \Mockery::mock(ResponseInterface::class);
        $response->shouldReceive('getBody')->withAnyArgs()->andReturns(
            file_get_contents(__DIR__ . '/../assets/fullprofile_v2.json')
        );

        $httpClient = \Mockery::mock(\GuzzleHttp\Client::class);
        $httpClient->shouldReceive('request')->withAnyArgs()->andReturns($response);


        $client = new Client('123','456');
        $client->setHttpClient($httpClient);
        $profile = $client->me($this->token);

        $this->assertInstanceOf(ProfileV2::class, $profile);
    }
}