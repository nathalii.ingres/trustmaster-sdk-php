<?php

namespace RESWUE\TrustTest;


use PHPUnit\Framework\TestCase;
use RESWUE\Trust\Profile\Location;
use RESWUE\Trust\Profile\Social;

class ProfileTest extends TestCase
{
    public function testFullProfile()
    {
        $data = json_decode(file_get_contents(__DIR__ . '/../assets/fullprofile.json'), true);
        $profile = new \RESWUE\Trust\Profile($data);

        $this->assertEquals('908afb20-814f-58fa-a9ac-81719fe599f5', $profile->getTrustmasterId());
        $this->assertEquals('RESWUE', $profile->getAgentName());
        $this->assertEquals('RESWUE', $profile->getName());
        $this->assertEquals('https://lh6.googleusercontent.com/-QyLXP7Ml4Eg/AAAAAAAAAAI/AAAAAAAAABI/cfZ6TST4sCU/photo.jpg?sz=50', $profile->getAvatar());
        $this->assertEquals('reswue@example.com', $profile->getEmail());
        $this->assertInstanceOf(Location::class, $profile->getLocation());
        $this->assertIsArray($profile->getLanguages());

        // location
        $this->assertInstanceOf(Location::class, $profile->getLocation());
        $this->assertEquals('58.009233', $profile->getLocation()->getLat());
        $this->assertEquals('-62.566417', $profile->getLocation()->getLng());
        $this->assertEquals('rounded', $profile->getLocation()->getPrivacy());

        // social profiles
        $this->assertInstanceOf(Social::class, $profile->getSocial());

        // google account 1
        $this->assertIsArray($profile->getSocial()->getGoogle());
        $this->assertInstanceOf(Social\Google::class, $profile->getSocial()->getGoogle()[0]);
        $this->assertEquals('RESWUE', $profile->getSocial()->getGoogle()[0]->getName());
        $this->assertEquals('reswue@example.com', $profile->getSocial()->getGoogle()[0]->getEmail());
        $this->assertEquals('https://lh6.googleusercontent.com/-QyLXP7Ml4Eg/AAAAAAAAAAI/AAAAAAAAABI/cfZ6TST4sCU/photo.jpg?sz=50', $profile->getSocial()->getGoogle()[0]->getAvatar());
        $this->assertEquals('110477265576843912947', $profile->getSocial()->getGoogle()[0]->getId());

        // google account 2
        $this->assertInstanceOf(Social\Google::class, $profile->getSocial()->getGoogle()[1]);
        $this->assertEquals('RESWUE 2', $profile->getSocial()->getGoogle()[1]->getName());
        $this->assertEquals('reswue2@example.com', $profile->getSocial()->getGoogle()[1]->getEmail());
        $this->assertEquals('https://lh6.googleusercontent.com/-QyLXP7Ml4Eg/AAAAAAAAAAI/AAAAAAAAABI/cfZ6TST4sCU/photo.jpg?sz=50', $profile->getSocial()->getGoogle()[0]->getAvatar());
        $this->assertEquals('110477265576843912947', $profile->getSocial()->getGoogle()[1]->getId());

        // telegram account
        $this->assertIsArray($profile->getSocial()->getTelegram());
        $this->assertInstanceOf(Social\Telegram::class, $profile->getSocial()->getTelegram()[0]);
        $this->assertEquals('0000000000', $profile->getSocial()->getTelegram()[0]->getId());
        $this->assertEquals('TrustmasterBot', $profile->getSocial()->getTelegram()[0]->getUsername());

        // zello account
        $this->assertIsArray($profile->getSocial()->getZello());
        $this->assertInstanceOf(Social\Zello::class, $profile->getSocial()->getZello()[0]);
        $this->assertEquals('RESWUE', $profile->getSocial()->getZello()[0]->getUsername());


    }
}